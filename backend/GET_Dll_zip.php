<?php
/*
 * Author : Janani Iyer
 *
 * This file will get all the dll, cpc and gs values for particular NE and return as JSON
 * It is to fill the values in the dll, cpc and gs dropdowns
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

if (!isset($_GET['valueOfNE']) || !isset($_GET['typeOfDll']))
	exit;

$neId = $_GET["valueOfNE"];
$dllId = $_GET["typeOfDll"];

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo json_encode("-1");
	// echo mysqli_connect_error();
	mysqli_close($connect);
	exit;
}

$queryFetchDll = "select dll_id, dll_ver from `pas_db`.`master_dll` where NE_id = '$neId' and dll_type = '$dllId'";

if(!$result= mysqli_query($connect, $queryFetchDll)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}
$dllarr = array();
while($row = mysqli_fetch_assoc($result)) {
	array_push($dllarr, $row);
}

//query2 - for CPC and GS dropdown
$queryFetchzip = "select zip_id, zip_ver, zip_type from `pas_db`.`master_zip` where NE_id = '$neId' and zip_id IN (select zip_id from `pas_db`.`mapping_dll_zip` a left join `pas_db`.`master_dll` b on a.dll_id = b.dll_id where b.dll_type = '$dllId')"; 

if(!$result= mysqli_query($connect, $queryFetchzip)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

$cpcarr = array();
$gsarr = array();
while($row = mysqli_fetch_assoc($result)) {
 
foreach($row as $key => $val) 
{
	if( $key == 'zip_type') {
		if($val == 1){
			//echo "<pre>";
 			//print_r ($row);
			array_push($cpcarr, $row);
			//print_r ($cpcarr);
		}
		else if($val == 2 ) {
		// add to gsarr
			array_push($gsarr, $row);
		}
	}
	else {
		//do nothing or throw error, which should not happen in the first place
	}
}
}
mysqli_close($connect);
echo json_encode(array('Dll' => $dllarr, 'Cpc' => $cpcarr, 'Gspf' => $gsarr));
exit;