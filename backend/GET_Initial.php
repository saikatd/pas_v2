<?php
/*
 * Author : Saikat De
 *
 * This file will get all setups and return as JSON
 */
 
// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo mysqli_connect_error();
}
$queryFetchSetup = "select id, setup from `pas_db`.`master_setup` where 1";
$result = mysqli_query($connect, $queryFetchSetup);

$returnValSetup = array();
while ($row = mysqli_fetch_assoc($result)) {
	array_push($returnValSetup, $row);
}

$queryFetchNE = "select NE_id, NE_name from `pas_db`.`master_ne` where 1";
$result = mysqli_query($connect, $queryFetchNE);

$returnValNE = array();
while ($row = mysqli_fetch_assoc($result)) {
	array_push($returnValNE, $row);
}

$queryFetchDoc = "select doc_name, doc_link from `pas_db`.`documents` where 1";
$result = mysqli_query($connect, $queryFetchDoc);

$returnValDoc = array();
while ($row = mysqli_fetch_assoc($result)) {
	array_push($returnValDoc, $row);
}


mysqli_close($connect);

echo json_encode(array('setup' => $returnValSetup, 'NE_list' => $returnValNE, 'Documents' => $returnValDoc));