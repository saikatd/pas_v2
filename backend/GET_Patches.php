<?php
/*
 * Author : Saikat De
 *
 * This file will get all patches related information and return them as JSON
 */

// Comment below two lines to hide errors

ini_set("display_errors","1");
error_reporting(E_ALL);

$ver_id = $_GET["valueOfVersion"];

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()){
	echo json_encode("-1");
	mysqli_close($connect);
	exit;
}

$queryGetColumns = "SELECT column_name FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA`='pas_db' AND `TABLE_NAME` = 'patches'";

if(!$result= mysqli_query($connect, $queryGetColumns)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

$selectFields = array();
while($row = mysqli_fetch_assoc($result)) {
	if($row['column_name'] != 'version_id') { 
		$selectFields[] = $row['column_name'];
	}
}
unset($result);

$selectFieldsStr = implode(",",$selectFields);

// get data from setup_sID
$queryFetchPatches = "select $selectFieldsStr from `pas_db`.`patches` where version_id = $ver_id";


if(!$result= mysqli_query($connect, $queryFetchPatches)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

while($row = mysqli_fetch_assoc($result)) {
	
	echo json_encode($row);
}
mysqli_close($connect);
exit;
