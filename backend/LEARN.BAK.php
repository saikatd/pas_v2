<?php
// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

/*$customerId = $_GET['valueOfCustomer'];
$typeOfDownloadId = $_GET['typeOfDownloadId'];
$typeOfDownload = $_GET['typeOfDownload'];*/

require_once "vars/dbvars.php";

try {
	$conn = mysqli_connect($host, $username, $password, "pas_db");
	if(mysqli_connect_errno()) {
		throw new Exception(mysqli_connect_error(), 1);
	}

	$q = "Show create table pas_db.master_communicator";

	if(!$result = mysqli_query($conn, $q)) {
		throw new Exception(mysqli_error($conn), 2);
	}
	while($row = mysqli_fetch_assoc($result)) {
		echo "<pre>";
		$createStatement = $row['Create Table'];
		$createStatement =  str_replace("communicator", "configurator", $createStatement);
		$createStatement =  str_replace("comm", "conf", $createStatement);
		if(!$resultOfCreate = mysqli_query($conn, $createStatement)) {
			throw new Exception(mysqli_error($conn), 2);
		}
	}


	mysqli_close($conn);
}
catch(Exception $error) {
	if($error->getCode() == 1) {
		echo "Could not connect to DB :: ".$error->getMessage();
	}
	if($error->getCode() == 2) {
		echo "Query Error :: ".$error->getMessage();
		mysqli_close($conn);
	}
}

exit;