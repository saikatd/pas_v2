<?php
/*
 * Author : Janani Iyer
 *
 * This file will get all NE values particular to customer and return as JSON
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---
if (!isset($_GET['valueOfNE']) || !isset($_GET['typeOfDll']) || !isset($_GET['valueOfDll']))
	exit;

$valueOfNE = $_GET["valueOfNE"];
$typeOfDll = $_GET["typeOfDll"];
$valueOfDll = $_GET["valueOfDll"];

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo json_encode("-1");
	// echo mysqli_connect_error();
	mysqli_close($connect);
	exit;
}

$queryGetDllDetails = "Select a.platform, a.NE_release, a.dll_name, a.dll_name_link, a.dll_RN, a.dll_RN_link, c.zip_name, c.zip_name_link, c.zip_RN, c.zip_RN_link, DATE_FORMAT(b.release_date, '%m-%d-%Y') as rDate from pas_db.master_dll a join pas_db.mapping_dll_zip b on a.dll_id=b.dll_id join pas_db.master_zip c on b.zip_id=c.zip_id where a.dll_id = $valueOfDll AND a.NE_id = $valueOfNE AND a.dll_type=$typeOfDll ORDER BY b.release_date DESC";
//echo ($queryGetNE);

if(!$result= mysqli_query($connect, $queryGetDllDetails)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

$resultsArr = array();

while($row = mysqli_fetch_assoc($result)) {
	$resultsArr[] = $row;
}

$sendArr = array();
foreach($resultsArr as $rowNo => $row) {
	$sendArr[] = array(
		'platform' => $row['platform'],
		'NE_release' => $row['NE_release'],
		'dll_name_pair' => array('dll_name'=>$row['dll_name'], 'dll_name_link' => $row['dll_name_link']),
		'dll_RN_pair' => array('dll_RN'=>$row['dll_RN'], 'dll_RN_link' => $row['dll_RN_link']),
		'zip_name_pair' => array('zip_name'=>$row['zip_name'], 'zip_name_link' => $row['zip_name_link']),
		'zip_RN_pair' => array('zip_RN'=>$row['zip_RN'], 'zip_RN_link' => $row['zip_RN_link']),
		'release_date' => $row['rDate'],
		);

}
//print_r($sendArr);
$sendValues = json_encode($sendArr);
echo $sendValues;

mysqli_close($connect);

exit;