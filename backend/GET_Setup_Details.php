<?php
/*
 * Author : Janani Iyer
 *
 * This file will get all downloads for particular setup and versionNo (passsed as get valuein URL) and return as JSON
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---

if (!isset($_GET['valueOfVersion']) || !isset($_GET['valueOfSetup']))
	exit;

$vId = $_GET["valueOfVersion"];
$sId = $_GET["valueOfSetup"];

require_once "vars/dbvars.php";

$connect = mysqli_connect($host, $username, $password);
if(mysqli_connect_errno()) {
	echo json_encode("-1");
	// echo mysqli_connect_error();
	mysqli_close($connect);
	exit;
}

$queryGetColumns = "SELECT column_name FROM `information_schema`.`COLUMNS` WHERE `TABLE_SCHEMA`='pas_db' AND `TABLE_NAME` = 'setup_$sId'";

if(!$result= mysqli_query($connect, $queryGetColumns)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

$selectFields = array();
while($row = mysqli_fetch_assoc($result)) {
	if($row['column_name'] != 'v_id') { 
		$selectFields[] = $row['column_name'];
	}
}
unset($result);

$selectFieldsStr = implode(",",$selectFields);

// get data from setup_sID
$queryFetchSetupDetails = "select $selectFieldsStr from `pas_db`.`setup_$sId` where v_id = $vId";


if(!$result= mysqli_query($connect, $queryFetchSetupDetails)) {
	echo json_encode(array("-1"));
	mysqli_close($connect);
	exit;
}

if(mysqli_num_rows($result)==0) {
	echo json_encode(array(0));
	mysqli_close($connect);
	exit;
}

while($row = mysqli_fetch_assoc($result)) {
	echo json_encode($row);
}

mysqli_close($connect);
exit;
