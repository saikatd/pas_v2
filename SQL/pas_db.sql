-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2014 at 08:08 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pas_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_name` varchar(100) NOT NULL,
  `doc_link` varchar(500) NOT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`doc_id`, `doc_name`, `doc_link`) VALUES
(1, 'Frequently Asked Questions', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3521500/Frequently%20Asked%20Questions_Draft4.doc'),
(2, 'PAS Training Video', 'http://video.all.alcatel-lucent.com/play/index/vid/9603'),
(3, 'Latest PAS User Guide', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511890/PAS%203.0.0%20UserGuide.doc'),
(4, 'PAS Quick Start Preparation Guide', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511888/PAS%20Quick%20Start%20Preparation%20Guide%20v2.docx'),
(5, 'PAS Tips for Integration Engineers', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511889/PAS%20Tips%20for%20Integration%20Engineers.xls'),
(6, 'Product Description Document', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511894/Product%20Description%20Document%20PAS%203%200%20Issue%201.pdf'),
(7, 'CPM PAS Requirements', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3167957/CPM Requirements v1 6 15012014-Erman updated.docx'),
(8, 'How To create PAS AR', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511893/SLA%20for%20PAS%20Support%20(How%20To%20create%20PAS%20AR)_V1.docx'),
(9, 'Create CARES AR for PAS License', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511887/How to Open CARES AR for PAS license request_v1 xls.xlsx');

-- --------------------------------------------------------

--
-- Table structure for table `mapping_dll_zip`
--

CREATE TABLE IF NOT EXISTS `mapping_dll_zip` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `dll_id` int(5) NOT NULL,
  `zip_id` int(5) NOT NULL,
  `release_date` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dll_id` (`dll_id`),
  KEY `zip_id` (`zip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mapping_dll_zip`
--

INSERT INTO `mapping_dll_zip` (`id`, `dll_id`, `zip_id`, `release_date`) VALUES
(1, 1, 1, '27-09-2013'),
(2, 1, 3, '7-10-13'),
(3, 3, 5, '13-10-1234'),
(4, 1, 2, '4-11-2014');

-- --------------------------------------------------------

--
-- Table structure for table `master_dll`
--

CREATE TABLE IF NOT EXISTS `master_dll` (
  `dll_id` int(5) NOT NULL AUTO_INCREMENT,
  `platform` varchar(100) NOT NULL,
  `NE_release` varchar(50) NOT NULL,
  `dll_ver` mediumint(9) NOT NULL,
  `dll_name` varchar(100) NOT NULL,
  `dll_name_link` varchar(500) NOT NULL,
  `dll_RN` varchar(100) NOT NULL,
  `dll_RN_link` varchar(500) NOT NULL,
  `NE_id` int(5) NOT NULL,
  `dll_type` int(11) NOT NULL,
  PRIMARY KEY (`dll_id`),
  KEY `dll_type` (`dll_type`),
  KEY `NE_id` (`NE_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `master_dll`
--

INSERT INTO `master_dll` (`dll_id`, `platform`, `NE_release`, `dll_ver`, `dll_name`, `dll_name_link`, `dll_RN`, `dll_RN_link`, `NE_id`, `dll_type`) VALUES
(1, 'abc', 'r4', 3456, 'hello', 'hellolink', 'helloRN', 'helloRNlink', 1, 1),
(2, 'adh', 'R5', 8901, 'abcnm', 'abcnmlink', 'abcnmRN', 'abcnmRNlink', 1, 1),
(3, 'bnm', 'R7', 9089, 'uidfhks', 'dsflk;sv', 'etyrg', 'ertertg', 2, 2),
(4, 'bnmasdfaf', 'R5', 6743, 'uidfhks', 'dsf', 'etyrg', 'ertertg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_dll_type`
--

CREATE TABLE IF NOT EXISTS `master_dll_type` (
  `dll_type_id` int(5) NOT NULL AUTO_INCREMENT,
  `dll_type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`dll_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_dll_type`
--

INSERT INTO `master_dll_type` (`dll_type_id`, `dll_type_name`) VALUES
(1, 'comm'),
(2, 'conf');

-- --------------------------------------------------------

--
-- Table structure for table `master_ne`
--

CREATE TABLE IF NOT EXISTS `master_ne` (
  `NE_id` int(5) NOT NULL AUTO_INCREMENT,
  `NE_name` varchar(20) NOT NULL,
  PRIMARY KEY (`NE_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `master_ne`
--

INSERT INTO `master_ne` (`NE_id`, `NE_name`) VALUES
(1, 'AAA'),
(2, 'COM'),
(3, 'CTS'),
(4, 'D-RTR'),
(5, 'IeCCF'),
(6, 'ISC-SBC'),
(7, 'MGC-8'),
(8, 'MGW/BGW'),
(9, 'MRF'),
(10, 'MRF-PRBT'),
(11, 'PS/XDMS'),
(12, 'RTR'),
(13, 'SCG'),
(14, 'SDM-BE'),
(15, 'SDM-EXPERT'),
(16, 'SDM-FE');

-- --------------------------------------------------------

--
-- Table structure for table `master_setup`
--

CREATE TABLE IF NOT EXISTS `master_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setup` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `master_setup`
--

INSERT INTO `master_setup` (`id`, `setup`) VALUES
(1, 'Laptop'),
(2, 'Standalone'),
(3, 'COM_VM'),
(4, 'Upgrade');

-- --------------------------------------------------------

--
-- Table structure for table `master_version`
--

CREATE TABLE IF NOT EXISTS `master_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versionNo` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `versionNo` (`versionNo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_version`
--

INSERT INTO `master_version` (`id`, `versionNo`) VALUES
(1, '2.3'),
(2, '2.3.1');

-- --------------------------------------------------------

--
-- Table structure for table `master_zip`
--

CREATE TABLE IF NOT EXISTS `master_zip` (
  `zip_id` int(5) NOT NULL AUTO_INCREMENT,
  `zip_ver` int(5) NOT NULL,
  `zip_name` varchar(50) NOT NULL,
  `zip_name_link` varchar(500) NOT NULL,
  `zip_RN` varchar(50) NOT NULL,
  `zip_RN_link` varchar(500) NOT NULL,
  `NE_id` int(5) NOT NULL,
  `zip_type` int(5) NOT NULL,
  PRIMARY KEY (`zip_id`),
  KEY `NE_id` (`NE_id`),
  KEY `zip_type` (`zip_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `master_zip`
--

INSERT INTO `master_zip` (`zip_id`, `zip_ver`, `zip_name`, `zip_name_link`, `zip_RN`, `zip_RN_link`, `NE_id`, `zip_type`) VALUES
(1, 23, 'zipname1', 'zipname1link', 'zipname1RN', 'zipname1RNlink', 1, 1),
(2, 12, 'zipname2', 'zipname2link', 'zipname2RN', 'zipname2RNlink', 1, 2),
(3, 67, 'zipname3', 'zipname3link', 'zipname3RN', 'zipname3RNlink', 1, 1),
(4, 88, 'zipname4', 'zipname4link', 'zipname4RN', 'zipname4RNlink', 1, 2),
(5, 45, 'zip5', 'zip5link', 'RN', 'RNlink', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_zip_type`
--

CREATE TABLE IF NOT EXISTS `master_zip_type` (
  `zip_type_id` int(5) NOT NULL AUTO_INCREMENT,
  `zip_type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`zip_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_zip_type`
--

INSERT INTO `master_zip_type` (`zip_type_id`, `zip_type_name`) VALUES
(1, 'cpc'),
(2, 'gspf');

-- --------------------------------------------------------

--
-- Table structure for table `patches`
--

CREATE TABLE IF NOT EXISTS `patches` (
  `version_id` int(5) NOT NULL,
  `Release_Notes` varchar(500) NOT NULL,
  `Database_Patch` varchar(500) NOT NULL,
  `Timeout_Patch` varchar(500) NOT NULL,
  `Package_Patch` varchar(500) NOT NULL,
  `Webpas_Patch` varchar(500) NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patches`
--

INSERT INTO `patches` (`version_id`, `Release_Notes`, `Database_Patch`, `Timeout_Patch`, `Package_Patch`, `Webpas_Patch`) VALUES
(1, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3419296/PAS%20R3.0.0%20Patches%20-%20Release%20Notes%20and%20Installation.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3511041/pas300_database_patch2.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3506259/pas300_timeout_patch.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3466018/pas300_package_patch_1.zip', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3488782/WebPAS.dll');

-- --------------------------------------------------------

--
-- Table structure for table `setup_1`
--

CREATE TABLE IF NOT EXISTS `setup_1` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `Installation_Guide` varchar(150) NOT NULL,
  `User_Guide` varchar(150) NOT NULL,
  `Release_Notes` varchar(150) NOT NULL,
  `Upgrade_guide` varchar(150) DEFAULT NULL,
  `Load_exe_File` varchar(150) NOT NULL,
  `Load_ova_File` varchar(150) NOT NULL,
  `Webser_Comm_if` varchar(150) NOT NULL,
  `Quickstart_prep` varchar(150) DEFAULT NULL,
  `Checksum` varchar(150) NOT NULL,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=3 ;

--
-- Dumping data for table `setup_1`
--

INSERT INTO `setup_1` (`v_id`, `Installation_Guide`, `User_Guide`, `Release_Notes`, `Upgrade_guide`, `Load_exe_File`, `Load_ova_File`, `Webser_Comm_if`, `Quickstart_prep`, `Checksum`) VALUES
(2, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072967/PAS%20Laptop%20VM%20Installation%20Guide%20v05.docx', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072505/PAS%202.3.1%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072519/Core%20Software%20Release%20Notes%20for%202-3-1.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-2864692/VMware-player-5.0.0-812388.exe', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3073374/vmpas231.ova', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072507/PAS%20Web%20Service%20Communication%20Interface_update5.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072526/Checksum_PAS2.3.1.txt'),
(1, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3037344/PAS%20Laptop%20VM%20Installation%20Guide%20v04.docx', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034123/PAS%202.3%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034276/Core%20Software%20Release%20Notes%20for%202-3.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-2864692/VMware-player-5.0.0-812388.exe', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3035490/vmpas23.ova', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039892/PAS%20Web%20Service%20Communication%20Interface_update4.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039116/Checksum_PAS2.3.txt');

-- --------------------------------------------------------

--
-- Table structure for table `setup_2`
--

CREATE TABLE IF NOT EXISTS `setup_2` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `Installation_Guide` varchar(150) NOT NULL,
  `User_Guide` varchar(150) NOT NULL,
  `Release_Notes` varchar(150) NOT NULL,
  `Upgrade_guide` varchar(150) DEFAULT NULL,
  `PA_Package_File` varchar(150) NOT NULL,
  `PAS_Database` varchar(150) NOT NULL,
  `Webser_Comm_if` varchar(150) NOT NULL,
  `Quickstart_prep` varchar(150) DEFAULT NULL,
  `Checksum` varchar(150) NOT NULL,
  PRIMARY KEY (`v_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=3 ;

--
-- Dumping data for table `setup_2`
--

INSERT INTO `setup_2` (`v_id`, `Installation_Guide`, `User_Guide`, `Release_Notes`, `Upgrade_guide`, `PA_Package_File`, `PAS_Database`, `Webser_Comm_if`, `Quickstart_prep`, `Checksum`) VALUES
(1, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039810/Parameter%20Audit%20SmartApp%20R2.3%20Installation%20and%20Configuration%20MOP.docx', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034123/PAS%202.3%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3034276/Core%20Software%20Release%20Notes%20for%202-3.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039812/ParameterAudit_2.3.PackageFile', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039906/pas_database.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039892/PAS%20Web%20Service%20Communication%20Interface_update4.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3039116/Checksum_PAS2.3.txt'),
(2, 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3074103/Parameter%20Audit%20SmartApp%20R2.3.1%20Installation%20and%20Configuration%20MOP.do', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072505/PAS%202.3.1%20UserGuide.doc', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072519/Core%20Software%20Release%20Notes%20for%202-3-1.doc', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3071308/ParameterAudit_2.3.1.131030.PackageFile', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3074100/pas_database.sql', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072507/PAS%20Web%20Service%20Communication%20Interface_update5.docx', ' ', 'http://docushare.web.alcatel-lucent.com/dsweb/Get/Document-3072526/Checksum_PAS2.3.1.txt');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mapping_dll_zip`
--
ALTER TABLE `mapping_dll_zip`
  ADD CONSTRAINT `mapping_dll_zip_ibfk_1` FOREIGN KEY (`dll_id`) REFERENCES `master_dll` (`dll_id`),
  ADD CONSTRAINT `mapping_dll_zip_ibfk_2` FOREIGN KEY (`zip_id`) REFERENCES `master_zip` (`zip_id`);

--
-- Constraints for table `master_dll`
--
ALTER TABLE `master_dll`
  ADD CONSTRAINT `master_dll_ibfk_2` FOREIGN KEY (`dll_type`) REFERENCES `master_dll_type` (`dll_type_id`),
  ADD CONSTRAINT `master_dll_ibfk_3` FOREIGN KEY (`NE_id`) REFERENCES `master_ne` (`NE_id`);

--
-- Constraints for table `master_zip`
--
ALTER TABLE `master_zip`
  ADD CONSTRAINT `master_zip_ibfk_1` FOREIGN KEY (`NE_id`) REFERENCES `master_ne` (`NE_id`),
  ADD CONSTRAINT `master_zip_ibfk_2` FOREIGN KEY (`zip_type`) REFERENCES `master_zip_type` (`zip_type_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
