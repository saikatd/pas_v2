<?php
/*
 * Author : Saikat De & Janani Iyer
 *
 * This file acts as the frontend interface to the module
 *
 *
 * 
 */

// Comment below two lines to hide errors
ini_set("display_errors", "1");
error_reporting(E_ALL);
// ---


?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>PAS Webpage</title>
	<script src="http://localhost/COMMON-INCLUDES/jquery/jquery.js"></script>
	<script src="http://localhost/pas_v2/jquery-ui/jquery-ui.js"></script>
	<link rel="stylesheet" href="http://localhost/pas_v2/jquery-ui/jquery-ui.css">
	<link rel="stylesheet" href="http://localhost/pas_v2/css/common.css">
	<script>
		$(document).ready(function() {
			$( "#tabs" ).tabs();
		});
	</script>
</head>
<body>
	<div id="root-container">	<!-- root container begins -->
		<div id="header">

		</div>

		<div id="content"> 	 <!-- logic here -->
			<div id="heading">
				<div id="heading_text">
					PAS Deliverables
				</div>
				<div id="heading_logo">
					<img src="images/logo.png">
				</div>
			</div> 
			<div id="tabs_container">
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1">PAS Documents</a></li>
						<li><a href="#tabs-2">PAS Builds</a></li>
						<li><a href="#tabs-3">Audit Communicator/Configurator</a></li>
						<li><a href="#tabs-4">Contact Us</a></li>
					</ul>

					<!-- PAS documents Tab --> 
					
					<div id="tabs-1">
						<div id= "docDetailsDiv">
						</div>
					</div>
					
					<!-- PAS Builds Tab -->

					<div id="tabs-2">
						<div class="selectors" id="div_pasbuilds">
							<div class="selectorGroupHeading">Download PAS Builds</div>

							<div>
								<label class="selectorsLabel" for="select_setup">Setup Type &raquo; </label>
								<select id="select_setup" onchange="fetch_version()"> <!-- <- on change remove links -->
									<option value="" selected>Select a Setup</option>
								</select>
							</div>

							<div>
								<label class="selectorsLabel" for="select_versionNo">PAS Version &raquo; </label>
								<select id="select_versionNo" onchange="fetch_setupinfo()">
									<option value="" selected>Select a Version</option>
								</select>
							</div>

						</div>
						<div id="info">
							<div>
								<div id="infoTableDiv">
								</div>
								<div id="patches">
								</div>
							</div>
						</div>
					</div>

					<!-- PAS communicator and Configurator download -->

					<div id="tabs-3">
						<div class="selectors" id="div_pascommunicators">
							<div class="selectorGroupHeading">Download PAS Communicators/Configurators</div>

							<div>
								<label size="20px" class="selectorsLabel" for="select_networkElement">NE Type &nbsp &raquo; </label>
								<select id="select_networkElement" onchange="empty_dropdowns()">
									<option value="" selected>Select a NE</option>
								</select>
							</div>

							<div>
								<div>
									<input type="radio" id = "radio_communicator" onchange="fetch_dll_zip()" name="choice" value="communicator">
									<label class="selectorsLabel" for="radio_communicator" OnClick="radio_check(1)">Communicator</label>

									<input type="radio" id="radio_configurator" onchange="fetch_dll_zip()" name="choice" value="configurator">
									<label class="selectorsLabel" for="radio_configurator" OnClick="radio_check(2)">Configurator</label>
								</div>
							</div>

							<div>
								<div id="dll" onmouseover="emphasize_disabled(1)">
									<label class="selectorsLabel" for="select_dll">DLL</label>
									<select id="select_dll" onchange="fetch_dll_details()">
										<option value="" selected>Select a Version</option>
									</select>
								</div>
								<div id="gspf_cpc" onmouseover="emphasize_disabled(2)">
									<div id="cpc">
										<label class="selectorsLabel" for="select_cpc">CPC</label>
										<select id="select_cpc" onchange="fetch_zip_details(1)">
											<option value="" selected>Select a Version</option>
										</select>
									</div>
									<div id="gspf">
										<label class="selectorsLabel" for="select_gspf">GSPF</label>
										<select id="select_gspf" onchange="fetch_zip_details(2)">
											<option value="" selected>Select a Version</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div id="info">
							<div>
								<div id="detailsTableDiv">
								</div>
								<div id="patches">
								</div>
							</div>
						</div>
					</div>

				<!-- Tab-4 Contact us Tab -->

					<div id="tabs-4">
						<div>
							<p>If you have any questions or need any support specific to this webpage, contact 
							<a href="mailto:pas-admin-support@list.alcatel-lucent.com?Subject=PAS Webpage " target="_top"><b>pas-admin-support</b></a>
							</p>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="footer">
			<div id="footer_logo">
				<img src="images/logo_footer.png">
			</div>
		</div>
		<script type="text/javascript">

// GLOBAL VARIABLES
var DOMAIN         = "localhost";
var DOMAINADDR     = "http://"+DOMAIN+"/";
var MODULEFOLDER   = "pas_v2";
var MODULEPATH     = DOMAINADDR+MODULEFOLDER+"/";
var BACKKENDFOLDER = "backend";
var BACKENDPATH    = MODULEPATH+BACKKENDFOLDER+"/";

// FUNCTION DEFINATIONS HERE

function empty_selectBox(selectBoxID) {
	var initialVal = $(selectBoxID + " option[value='']").text();
	var optionString = "<option value=\"\" selected>"+initialVal+"</option>";
	$(selectBoxID).html(optionString);
}

function defaulting_select(selectBoxID) {
	$(selectBoxID).val('');
}

function is_null(optionValue) {
	if(optionValue == null || optionValue == 'undefined' || optionValue == "") 
		return 1;
	else 
		return 0;
}

/*Working on Tab 2 from here*/

function fetch_version() {
	// check if value is empty
	var valueOfSetup = $('#select_setup').find(':selected').attr('value');
	if (is_null(valueOfSetup)) {
		return;
	}

	$("#infoTableDiv").empty();
	$("#patches").empty();
	//console.log(valueOfSetup);
	$.ajax({ 	// call backend and fetch data using ajax
		url: BACKENDPATH + "GET_Version.php",
		dataType : "JSON",

		success: function(response) {
			// console.log("---");
			// console.log(response);

			empty_selectBox("#select_versionNo");

			$.each(response, function(i, val) {
				var selOpt = document.createElement("option");
				selOpt.text = val['versionNo'];
				selOpt.value = val['id'];
				document.getElementById("select_versionNo").add(selOpt);
			});
		},
	});
}
//////////////////////

function patch_show(){
	
	var valueOfVersion = $('#select_versionNo').find(':selected').attr('value');

	if(is_null(valueOfVersion))
		return;
	
	$("#patches").empty();

	$.ajax ({
		url : BACKENDPATH + "GET_Patches.php?valueOfVersion="+valueOfVersion,
		dataType : "JSON",

		success: function(response) {
			// console.log(response);
			if(response == 0) {
				$("#patches").html("Sorry, No patches found.");

				return;
			}
			if(response == -1) {
				$("#patches").html("Sorry, your data could not be fetched.");
				return;
			}

			var rowString = "";
			rowString += "<div class='header_row' data-forfield='patches' data-celldisptype='regular'><b>PATCHES</b></div>";

			$.each(response, function(i, val) {
				rowString += "<div class='infoTableCell' data-celltype='"+i+"'><a class='download' href=" +val+ ">" +i+ "</a></div>";
			});

			$("#patches").css("display", "none").html(rowString).fadeIn(1000);
		}
	});
} 
/////////////////////

function fetch_setupinfo () {
	var valueOfVersion = $('#select_versionNo').find(':selected').attr('value');
	var valueOfSetup = $('#select_setup').find(':selected').attr('value');

	if (is_null(valueOfVersion) ||  is_null(valueOfSetup)) {
		return;
	}

	$.ajax({
		url: BACKENDPATH + "GET_Setup_Details.php?valueOfVersion="+valueOfVersion+"&valueOfSetup="+valueOfSetup,
		dataType: "JSON",

		success: function(response) {
			// console.log(response);
			if(response == 0) {
				$("#infoTableDiv").html("Sorry, No data found.");

				return;
			}
			if(response == -1) {
				$("#infoTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}

			var rowString = "";
			rowString += "<div class='header_row' data-forfield='Builds' data-celldisptype='regular'><b>BUILDS</b></div>";
			var counter =1;
			$.each(response, function(i, val) {
				if(counter == 10){
				rowString += "<div class='infoTableCell' data-celltype='"+i+"'>"+val+"</div>";
				}
				else{
				rowString += "<div class='infoTableCell' data-celltype='"+i+"'><a class='download' href=" +val+ ">" +i+ "</a></div><br>";
				}
				counter = counter +1;
			});

			$("#infoTableDiv").css("display", "none").html(rowString).fadeIn(1000);
			patch_show();
		}
	});

	/*Working on Initial Fetch from Here*/

}
// DOCUMENT READY

$(document).ready(function() { 	/* documnet is ready now */
	$.ajax({ 	// call backend and fetch data using ajax
		url: BACKENDPATH + "GET_Initial.php",
		dataType : "json",

		success: function(response) {
			var setup = response['setup'];
			var NE_list = response['NE_list'];
			var Documents = response['Documents'];

			empty_selectBox("#select_setup");
			empty_selectBox("#select_networkElement"); // <- convert this function so that it accepts an array. array of any number of divs.

			// <- write a function that takes an array in the below format, and populates the selectboxes.
			// i.e. function create_selectboxOptions(arrayInThisFormat, selectboxId)
			$.each(setup, function(i, val) {		
				var valOfOption = val['id'];

				var selOpt = document.createElement("option");
				selOpt.text = val['setup'];
				selOpt.value = valOfOption;
				document.getElementById("select_setup").add(selOpt);
			});

			$.each(NE_list, function(i, val) {
				var valOfOption = val['NE_id'];

				var selOpt = document.createElement("option");
				selOpt.text = val['NE_name'];
				selOpt.value = valOfOption;
				document.getElementById("select_networkElement").add(selOpt);
			});

			$.each(Documents,function(i,val){
				rowString = "";
				rowString += "<div class='infoTableCell' data-celltype='"+val['doc_name']+"'><a class='download' href=" +val['doc_link']+ ">" +val['doc_name']+ "</a></div>";
				rowString += "</div><br>";
				$('#docDetailsDiv').append(rowString);
			});
		}, //success function ends
	});	// ajax ends
});		// document.ready ends

/*Working on Tab3 from Here*/

function empty_dropdowns(){
	empty_selectBox("#select_dll");
	empty_selectBox("#select_cpc"); 
	empty_selectBox("#select_gspf"); 
	$("#radio_communicator").attr("checked", false)
	$("#radio_configurator").attr("checked", false)
}

/*function radio_check(dll_type){
	if (dll_type==1){
		$("#radio_communicator").attr("checked", true)
	}
	if (dll_type == 2){
		$("#radio_configurator").attr("checked", true)
	}
}
*/
function fetch_dll_zip () {
	var valueOfNE = $('#select_networkElement').find(':selected').attr('value');
	
	if (document.getElementById("radio_communicator").checked == true){
		var typeOfDll = 1;
	}
	if(document.getElementById("radio_configurator").checked == true){
		var typeOfDll = 2;
	}

	if (is_null(valueOfNE) ||  is_null(typeOfDll)) {
		return;
	}

	$.ajax({
		url: BACKENDPATH + "GET_Dll_zip.php?valueOfNE="+valueOfNE+"&typeOfDll="+typeOfDll,
		dataType: "JSON",
		success: function(response) {
			var Dll = response['Dll'];
			var Cpc = response['Cpc'];
			var Gspf = response['Gspf'];

			empty_selectBox("#select_dll");
			empty_selectBox("#select_cpc"); 
			empty_selectBox("#select_gspf"); 

			$.each(Dll, function(i, val) {		
				var valOfOption = val['dll_id'];
				
				var selOpt = document.createElement("option");
				selOpt.text = "v"+val['dll_ver'];
				selOpt.value = valOfOption;
				document.getElementById("select_dll").add(selOpt);
			});

			$.each(Cpc, function(i, val) {		
				var valOfOption = val['zip_id'];
				
				var selOpt = document.createElement("option");
				selOpt.text = "MFV"+val['zip_ver'];
				selOpt.value = valOfOption;
				document.getElementById("select_cpc").add(selOpt);
			});

			$.each(Gspf, function(i, val) {		
				var valOfOption = val['zip_id'];
				
				var selOpt = document.createElement("option");
				selOpt.text = "MFV"+val['zip_ver'];
				selOpt.value = valOfOption;
				document.getElementById("select_gspf").add(selOpt);
			});
		},
	});	// ajax ends
}

function fetch_dll_details() {
	var valueOfNE = $('#select_networkElement').find(':selected').attr('value');

	if (document.getElementById("radio_communicator").checked == true){
		var typeOfDll = 1;
		var dTypeFull ="Communicator";
		var dTypeShort ="Comm";
	}
	if(document.getElementById("radio_configurator").checked == true){
		var typeOfDll = 2;
		var dTypeFull ="Configurator";
		var dTypeShort ="Conf";
	}

	var valueOfDll = $('#select_dll').find(':selected').attr('value');

	if (is_null(valueOfNE) ||  is_null(valueOfDll) || is_null(typeOfDll)) {
		return;
	}

	$("#DetailTableDiv").empty();
	defaulting_select(select_gspf);
	defaulting_select(select_cpc);
	$.ajax({
		url: BACKENDPATH + "GET_Dll_details.php?valueOfNE="+valueOfNE+"&typeOfDll="+typeOfDll+"&valueOfDll="+valueOfDll,
		dataType: "JSON",

		success : function(response) {
			// do stuff 
			if(response == 0) {
				$("#detailsTableDiv").html("Sorry, No data found.");
				return;
			}

			if(response == -1) {
				$("#detailsTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}
			
			$('#detailsTableDiv').html('');

			htmlheader = "";
			htmlheader += "<div class='header_row' id='header_row' data-forDll='"+typeOfDll+"'>"; 
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='platform'>Platform</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='NE_release'>NE Release</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+dTypeShort+"_name'>"+dTypeFull+" Name</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+dTypeShort+"_RN'>"+dTypeFull+" Release Notes</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='cpc_name'>CPC/GS Name</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='cpc_RN'>CPC/GS Release Notes</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='release_date'>Release Date</div>";
			htmlheader += "</div>";

			$('#detailsTableDiv').append(htmlheader);
			
			var rowString = "";
			$.each(response, function(i, val) { 
				rowHTML = "<div class='data_row'>";
				$.each(val, function(fname,fvalue) {
					if(fname.indexOf("_pair") != -1) {		// find _pair in fname
						var pairIndex = [];
						var pairVals = [];
						$.each(fvalue, function(pIndex, iVal) {
							pairIndex.push(pIndex); 	// <- Just in case later
							pairVals.push(iVal);
						});
						rowHTML += "<div class='data_cell' data-cellDispType='link' data-cellType='"+fname+"' data-pairFor='"+pairIndex[0]+"'><a href="+pairVals[1]+" target=\"_blank\">"+pairVals[0]+"</a></div>";
					}
					else {		// no _pair in fname
						rowHTML += "<div class='data_cell' data-cellDispType='regular' data-cellType='"+fname+"'>"+fvalue+"</div>";
					}
				});
				rowHTML += "</div><br><br>";
				$('#detailsTableDiv').append(rowHTML);
			});		// $.each ends
		},		// success function ends
	}); //ajax closing
} // function fetch_dll_details closing

function fetch_zip_details(typeOfZip){
var valueOfNE = $('#select_networkElement').find(':selected').attr('value');

	if (document.getElementById("radio_communicator").checked == true){
		var typeOfDll = 1;
		var dTypeFull ="Communicator";
		var dTypeShort ="Comm";
	}
	if(document.getElementById("radio_configurator").checked == true){
		var typeOfDll = 2;
		var dTypeFull ="Configurator";
		var dTypeShort ="Conf";
	}

	if(typeOfZip == 1){
		var valueOfZip = $('#select_cpc').find(':selected').attr('value');
		var zTypeFull ="CPC";
		defaulting_select(select_gspf);

	}
	if(typeOfZip == 2){
		var valueOfZip = $('#select_gspf').find(':selected').attr('value');
		var zTypeFull ="GSPF";
		defaulting_select(select_cpc);
	}
	
	if (is_null(valueOfNE) ||  is_null(valueOfZip) || is_null(typeOfDll) || is_null(typeOfZip)){
		return;
	}

	$("#DetailTableDiv").empty();
	defaulting_select(select_dll);
	
	
	$.ajax({
		url: BACKENDPATH + "GET_Zip_details.php?valueOfNE="+valueOfNE+"&typeOfDll="+typeOfDll+"&valueOfZip="+valueOfZip+"&typeOfZip="+typeOfZip,
		dataType: "JSON",

		success : function(response) {
			// do stuff 
			if(response == 0) {
				$("#detailsTableDiv").html("Sorry, No data found.");
				return;
			}

			if(response == -1) {
				$("#detailsTableDiv").html("Sorry, your data could not be fetched.");
				return;
			}
			
			$('#detailsTableDiv').html('');

			htmlheader = "";
			htmlheader += "<div class='header_row' id='header_row' data-forDll='"+typeOfDll+"'>"; 
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+zTypeFull+"_name'>"+zTypeFull+" Name</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+zTypeFull+"_RN'>"+zTypeFull+" Release Notes</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+dTypeShort+"_name'>"+dTypeFull+" Name</div>";
			htmlheader += "<div class='header_cell' data-ispair = '1' data-cellDispType='link' data-forField='"+dTypeShort+"_RN'>"+dTypeFull+" Release Notes</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='platform'>Platform</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='NE_release'>NE Release</div>";
			htmlheader += "<div class='header_cell' data-cellDispType='regular' data-forField='release_date'>Release Date</div>";
			htmlheader += "</div>";

			$('#detailsTableDiv').append(htmlheader);
			
			var rowString = "";
			$.each(response, function(i, val) { 
				rowHTML = "<div class='data_row'>";
				$.each(val, function(fname,fvalue) {
					if(fname.indexOf("_pair") != -1) {		// find _pair in fname
						var pairIndex = [];
						var pairVals = [];
						$.each(fvalue, function(pIndex, iVal) {
							pairIndex.push(pIndex); 	// <- Just in case later
							pairVals.push(iVal);
						});
						rowHTML += "<div class='data_cell' data-cellDispType='link' data-cellType='"+fname+"' data-pairFor='"+pairIndex[0]+"'><a href="+pairVals[1]+" target=\"_blank\">"+pairVals[0]+"</a></div>";
					}
					else {		// no _pair in fname
						rowHTML += "<div class='data_cell' data-cellDispType='regular' data-cellType='"+fname+"'>"+fvalue+"</div>";
					}
				});
				rowHTML += "</div><br><br>";
				$('#detailsTableDiv').append(rowHTML);
			});		// $.each ends
		},		// success function ends
	}); //ajax closing
}//function fetch_zip_details closing

function emphasize_disabled(divtype) {
	if(divtype==1) {
		var thisDivId = "#dll";
		var enemyDivId = "#gspf_cpc";
	}
	else if(divtype==2) {
		var enemyDivId = "#dll";
		var thisDivId = "#gspf_cpc";
	}
	else {
		// no div for this yet
	}
	// $(enemyDivId).wrap("<div class='disabled_overlay'></div>").find("select").addClass("disabled");
	// $(thisDivId).mouseleave(function() {
	// 	if($(enemyDivId).parent().attr("class") == "disabled_overlay") {
	// 		$(enemyDivId).unwrap().find("select").removeClass("disabled");
	// 	}
	// 	else {
	// 		$(enemyDivId).find("select").removeClass("disabled");
	// 	}
	// });
	$(enemyDivId).addClass("disabled_overlay").find("select").addClass("disabled");
	$(thisDivId).mouseleave(function() {
		$(enemyDivId).removeClass("disabled_overlay").find("select").removeClass("disabled");
	});
}
</script>
</body>
</html>